Hledger 1.1
===========

A Docker container for running hledger 1.1
Assumes `$LEDGER_FILE` is set in your host environment.


Usage:
------

LEDGER_DIR=$(realpath $(dirname $LEDGER_FILE));
docker run -it -v $LEDGER_DIR:/data:ro 0x0398/hledger -f /data/$(basename $LEDGER_FILE) <commands>
